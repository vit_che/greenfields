<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateService;
use App\Field;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dateCheck()
    {
//        dump("test");
//
//        dump(DateService::isValid('23/02/1999'));

        return view('tests.test_page');
    }


    public function customMethod()
    {
        $fi = Field::where('id', '>', 9)->take(2)->get();
        if ($fi) {
            dump($fi);
        } else {
            dump("FALSE");
        }

        return ;
    }

    public function customFunc()
    {
        return "Custom Func";
    }

}
