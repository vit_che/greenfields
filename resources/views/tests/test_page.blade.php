@extends('layouts.basic')

@section('test')
    <br>
    <div class="flex-center position-ref full-height">
        <div class="text-center">
            <a class="btn btn-secondary" href="{{ route('home') }}">Back to Home Page</a>
        </div>
    </div>
    <br>

    @if($field)

        <div class="text-center">{{ $field->name }}</div>

    @endif


@endsection
